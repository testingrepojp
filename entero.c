#include <stdio.h>
#include <string.h>

typedef struct{
    signed char losDigitos[ BUFSIZ ];
    size_t      laCantidadDeDigitos;
} Entero;

Entero Entero_CrearDesdeCadena(char *unaCadenaConUnNumero);
Entero Entero_Sumar(Entero unEntero, Entero otroEntero);
Entero Entero_Read(FILE *in);
int Entero_Write(const Entero unEntero, FILE *out);
int Entero_Scan(FILE *in, Entero *unEntero);
void Entero_Print(Entero unEntero, FILE *out);
int Entero_Comparar(Entero unEntero, Entero otroEntero);

static void strEspejo(char *s, int len);
static int EsEntero(const char *unaCadena);
static int ColumnaAutomata(const char c);

#Test comment
Entero Entero_CrearDesdeCadena(char *unaCadenaConUnNumero)
{
  Entero temp;
  char *s = unaCadenaConUnNumero;
  int CantDig = strlen(unaCadenaConUnNumero);

  /*
   *  Le saco el signo más y los puntos, si es que los tiene.
   */
  if (*s == '+') s++;
  if ( s[CantDig - 4] == '.' ) {
    int i,k = 0;
    char t[ BUFSIZ ];
    for(i = 0; s[i] != '\0' ; i++)
      if (s[i] != '.'){
	CantDig--;
	t[k++] = s[i];
      }
    t[k] = '\0';
    s = t;
  }

  strcpy(temp.losDigitos, s);
  temp.laCantidadDeDigitos = CantDig;
  return temp;
}


Entero Entero_Sumar(Entero unEntero, Entero otroEntero)
{
  //To Do.
}


static void strEspejo(char *s, int len)
{
  int i, temp;
  len--;

  for(i=0; i<len; i++, len--){
    temp=s[i];
    s[i]=s[len];
    s[len]=temp;
  }
}


int Entero_Comparar(Entero unEntero, Entero otroEntero)
{
  return strcmp(unEntero.losDigitos, otroEntero.losDigitos);
}


Entero Entero_Read(FILE *in)
{
  Entero temp;

  fread(&temp, sizeof temp, 1, in);

  return temp;
}


int Entero_Write(const Entero unEntero, FILE *out)
{
  fwrite(&unEntero, sizeof unEntero, 1, out);
  return 0;
}


void Entero_Print(Entero unEntero, FILE *out)
{
  if(EsEntero(unEntero.losDigitos))
    fprintf(out,"%s;",unEntero.losDigitos);
}


int Entero_Scan(FILE *in, Entero *unEntero)
{
  int c, i=0;
  char s[ BUFSIZ ];

  while( ((c = getc(in)) != ';') && (c != '\n') && (c != EOF) )
    s[i++] = c;
  s[i] = '\0';

  if( (c == ';') && EsEntero(s) )
    *unEntero = Entero_CrearDesdeCadena(s);
  else
    *unEntero = Entero_CrearDesdeCadena("");

  return c;
}

/*
 *    | 0  1..9  +,-  .  default
 *  --|------------------------
 *  0-| 10   2    1   -1   -1
 *  1 | -1   2   -1   -1   -1
 *  2+|  3   3   -1    6   -1
 *  3+|  4   4   -1    6   -1
 *  4+|  5   5   -1    6   -1
 *  5+|  5   5   -1   -1   -1
 *  6 |  7   7   -1   -1   -1
 *  7 |  8   8   -1   -1   -1
 *  8 |  9   9   -1   -1   -1
 *  9+| -1  -1   -1    6   -1
 * 10+| -1  -1   -1   -1   -1
 *
 * -1 es estado de rechazo directo.
 */

static int EsEntero(const char *unaCadena)
{
  static const int matrizAF[11][5]={{10,2,1,-1,-1},
				    {-1,2,-1,-1,-1},
				    {3,3,-1,6,-1},
				    {4,4,-1,6,-1},
				    {5,5,-1,6,-1},
				    {5,5,-1,-1,-1},
				    {7,7,-1,-1,-1},
				    {8,8,-1,-1,-1},
				    {9,9,-1,-1,-1},
				    {-1,-1,-1,6,-1},
				    {-1,-1,-1,-1,-1}};

  int i, estadoactual = 0;

  for( i=0 ; (unaCadena[i]!='\0') && (estadoactual!=-1) ; i++)
    estadoactual = matrizAF[ estadoactual ][ ColumnaAutomata(unaCadena[i]) ];

  if ( (estadoactual==2) || (estadoactual==3) ||
       (estadoactual==4) || (estadoactual==5) ||
       (estadoactual==9) || (estadoactual==10) ) return 1; //Es Reconocido
  return 0;
}


static int ColumnaAutomata(const char c)
{
  if (c=='0')
    return 0; 
  else if ( (c>='1') && (c<='9') )
    return 1;
  else if ( (c=='+') || (c=='-') )
    return 2;
  else if ( c=='.' )
    return 3;
  else
    return 4; //Con esta opcion, la matriz siempre devuelve -1, NO ACEPTADA.
}
